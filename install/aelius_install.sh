#!/bin/bash

echo -e "\n# Baixando Aelius...\n"

wget https://www.dropbox.com/s/zeu3rvg4v46ftjg/aelius.zip

echo -e "\n# Extraindo...\n"

unzip aelius.zip 

echo -e "\n# Instalando dependências...\n"

sudo apt-get update
sudo apt-get install -y python-dev python-pip python-yaml python-numpy python-matplotlib
sudo pip install nltk==3.0.5 nltk_tgrep --upgrade

echo -e "\n# Setando variáveis de ambiente...\n"

echo -e "\nHUNPOS_TAGGER=\"$HOME/aelius/bin/hunpos-tag\"" >> ~/.bashrc
echo -e "export HUNPOS_TAGGER" >> ~/.bashrc

echo -e "\nAELIUS_DATA=\"$HOME/aelius/aelius_data\"" >> ~/.bashrc
echo -e "export AELIUS_DATA" >> ~/.bashrc

echo -e "\nNLTK_DATA=\"$HOME/aelius/nltk_data\"" >> ~/.bashrc
echo -e "export NLTK_DATA" >> ~/.bashrc

echo -e "\nPYTHONPATH=\"$HOME/aelius\"" >> ~/.bashrc
echo -e "export PYTHONPATH\n" >> ~/.bashrc

echo -e "# Execute o seguinte comando para finalizar:\n\n$ source ~/.bashrc\n"
# -*- coding: utf-8 -*-
#!/usr/bin/python

from lexicAnalyser import *

"""
    Authors: Jaelson Carvalho, Wesnydy Ribeiro
    Python >= 2.7
    Usage: python sintaxAnalyser.py
"""

index = -1
tupleList = []
nextTuple = None
currentTuple = None

def sentenca():
    sujeito()
    predicado()

def sujeito():
    print "\nSujeito:",
    global nextTuple
    nextTuple = lookAhead()
    if nextTuple is not None:
        if nextTuple[1] == "substantivo":
            substantivo()
        elif nextTuple[1] == "artigo":
            artigo()
            nextTuple = lookAhead()
            if nextTuple is not None:
                if nextTuple[1] == "substantivo":
                    substantivo()
                    nextTuple = lookAhead()
                    if nextTuple[1] == "adjetivo":
                        adjetivo()
                elif nextTuple[1] == "adjetivo":
                    adjetivo()
                    substantivo()
                else:
                    print "\nErro no sujeito: " + str(nextTuple) + " não reconhecido"
        else:
            print "nenhum",
    else:
        print "\nErro no sujeito: " + str(nextTuple) + " não reconhecido"

def predicado():
    print "\nPredicado:",
    global nextTuple
    nextTuple = lookAhead()
    if nextTuple is not None:
        if nextTuple[1] == "verbo":
            verbo()
            nextTuple = lookAhead()
            if nextTuple is not None:
                if nextTuple[1] == "verbo":
                    verbo()
                    objeto()
                else:
                    objeto()
            else:
                print "\nErro no predicado: Falta objeto"
        else:
            print "\nErro no predicado: " + str(nextTuple) + " não reconhecido"
    else:
        print "\nErro no predicado: " + str(nextTuple) + " não reconhecido"

def substantivo():
    global currentTuple
    if lookAhead() is not None:
        currentTuple = next()
        print currentTuple[0], # print o substantivo

def artigo():
    global currentTuple
    if lookAhead() is not None:
        currentTuple = next()
        print currentTuple[0], # print o artigo

def adjetivo():
    global currentTuple
    if lookAhead() is not None:
        currentTuple = next()
        print currentTuple[0], # print o adjetivo

def preposicao():
    global currentTuple
    if lookAhead() is not None:
        currentTuple = next()
        print currentTuple[0], # print a preposicao

def verbo():
    global currentTuple
    if lookAhead() is not None:
        currentTuple = next()
        print currentTuple[0], # print o verbo

def objeto():
    global nextTuple
    nextTuple = lookAhead()
    if nextTuple is not None:
        if nextTuple[1] == "substantivo":
            substantivo()
            nextTuple = lookAhead()
            if nextTuple is not None:
                objeto()
        elif nextTuple[1] == "artigo":
            artigo()
            nextTuple = lookAhead()
            if nextTuple is not None:
                if nextTuple[1] == "substantivo":
                    substantivo()
                elif nextTuple[1] == "adjetivo":
                    adjetivo()
                    substantivo()
                else:
                    print "\nErro no objeto: " + str(nextTuple) + " não reconhecido"
                nextTuple = lookAhead()
                if nextTuple is not None:
                    objeto()
        elif nextTuple[1] == "adjetivo":
            adjetivo()
            substantivo()
            nextTuple = lookAhead()
            if nextTuple is not None:
                objeto()

        elif nextTuple[1] == "preposicao":
            preposicao()
            substantivo()
            nextTuple = lookAhead()
            if nextTuple is not None:
                objeto()
        else:
            print "\nErro no objeto: " + str(nextTuple) + " não reconhecido"

def lookAhead():
    try:
        return tupleList[index+1]
    except:
        return None

def next():
    global index
    index = index + 1
    try:
       return tupleList[index]
    except:
        return None

def runSintax(listOfTuples):
    global tupleList
    tupleList = listOfTuples
    sentenca()
# -*- coding: utf-8 -*-
#!/usr/bin/python

import json
import nltk 
from Aelius import AnotaCorpus, Extras, Toqueniza

"""
    Authors: Jaelson Carvalho, Wesnydy Ribeiro
    Python >= 2.7
    Usage: python lexicAnalyser.py
"""

infinitiveFlag = False
# COLOCAR O CAMINHO DO ARQUIVO tags.json DA SUA MÁQUINA
jsonFile = "/home/wesnydy/Workspace/Dev/compiladores-2016.1/tags.json"

def tokenizeSentence(sentence):
    sentence = sentence.decode("utf-8")
    sents = Toqueniza.TOK_PORT.tokenize(sentence)
    return sents

def reinterpretTags(taggedSents):
    with open(jsonFile) as dataFile:
        ptTags = json.load(dataFile)

    global infinitiveFlag
    tupleList = []

    for word, tag in taggedSents[0]:
        infinitiveFlag = False
        if tag in ptTags["verbo"]:
            if tag not in ptTags["verboinf"]:
                infinitiveFlag = True
            tupleList.append((word, "verbo", infinitiveFlag))
        elif tag in ptTags["artigo"]:
            tupleList.append((word, "artigo", infinitiveFlag))
        elif tag in ptTags["adjetivo"]:
            tupleList.append((word, "adjetivo", infinitiveFlag))
        elif tag in ptTags["adverbio"]:
            tupleList.append((word, "adverbio", infinitiveFlag))
        # elif tag in ptTags["cardinal"]:
        #     tupleList.append((word, "cardinal"))
        # elif tag in ptTags["conjuncao"]:
        #     tupleList.append((word, "conjuncao"))
        elif tag in ptTags["substantivo"]:
            tupleList.append((word, "substantivo", infinitiveFlag))
        elif tag in ptTags["pontuacao"]:
            tupleList.append((word, "pontuacao", infinitiveFlag))
        elif tag in ptTags["preposicao"]:
            tupleList.append((word, "preposicao", infinitiveFlag))
        # elif tag in ptTags["pronome"]:
        #     tupleList.append((word, "pronome"))
        # elif tag in ptTags["quantificador"]:
        #     tupleList.append((word, "quantificador"))
        else:
            tupleList.append((word, "nenhum", infinitiveFlag))

    return tupleList

def tagSentences(sents):
    tagger = Extras.carrega("AeliusHunPos")
    taggedSentences = AnotaCorpus.anota_sentencas([sents], tagger, "hunpos")
    return reinterpretTags(taggedSentences)

def runLexic(sentence):
    tokens = tokenizeSentence(sentence)
    result = tagSentences(tokens)

    print "\nLista de tokens classificados:"
    for w, t, u in result:
        print "('%s', '%s')" % (w, t)

    return result
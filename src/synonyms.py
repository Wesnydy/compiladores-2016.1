from bs4 import BeautifulSoup
import urllib.request
import re
from string import punctuation
from unicodedata import normalize
import sys

"""
    Authors: Jaelson Carvalho, Wesnydy Ribeiro
    Python 3.0
    Usage: python synonyms.py
"""

def remove_word_accents(text):
  return normalize('NFKD', text).encode('ASCII','ignore').decode('ASCII').lower()

def word_synonyms(word):
  """[WARNING] ==> This function doesn't solve words derived from verbs"""
  with urllib.request.urlopen('http://www.sinonimos.com.br/' + remove_word_accents(word) + '/') as response:
     html = response.read()

  soup = BeautifulSoup(html, 'html.parser')
  synonyms_tag = soup.find_all('a')
  synonyms = []

  if len(synonyms_tag) > 0:
    for x in range(len(synonyms_tag)):
      if synonyms_tag[x].has_attr("class") and synonyms_tag[x].get("class")[0] == 'sinonimo':
        synonyms.append(synonyms_tag[x].get_text())

  synonyms.append(word.lower())
  return synonyms

def sentece_synonyms(sentence):
  """[WARNING] ==> This function doesn't solve words derived from verbs"""
  print(sentence, "\n")
  synonyms = {}
  regex = re.compile(r'[{}]'.format(punctuation))
  new_sentence = regex.sub(' ',sentence)

  splited_sentence = new_sentence.split()

  for word in splited_sentence:
    synonyms[word] = word_synonyms(word)

  return synonyms


command_words = ["Abrir", "Navegador", "Ativar", "opção", "de", "busca", "gato", "comer", "rato", "concluir", "ligação", "Fazer", "para", "Ligar", "Cancelar", "Encerrar"]

def verb_derived_word_parser(word):
  with urllib.request.urlopen('https://www.conjugacao.com.br/busca.php?q=' + remove_word_accents(word)) as response:
     html = response.read()

  verb = BeautifulSoup(html, 'html.parser').find(id="content").find_all('a')

  if len(verb) > 0:
    return remove_word_accents(verb[0].get_text().lower())
  else:
    print (verb)
    return "Error"


def word_validation(word, verb_derived_word):
  if verb_derived_word:
    word = verb_derived_word_parser(word)

  for synonym in word_synonyms(word):
    if remove_word_accents(synonym) in map(lambda x:remove_word_accents(x),command_words):
      return True
  return False

"""
# Just testing the sentece_synonyms function
sentence = "Desligar a TV."

a = sentece_synonyms(sentence)
for x in a:
  print(x,"==>", a[x], '\n')
"""

"""
# Just testing the verb_derived_word_parser function
print (verb_derived_word_parser("Desligando"))
"""

"""
# Just testing the word_validation function
print(word_validation("Desligando", True))
"""

def str_to_bool(s):
    if s == 'True':
         return True
    elif s == 'False':
         return False
    else:
         raise ValueError # evil ValueError that doesn't tell you what the wrong value was

if (len(sys.argv) == 3):
  is_infinitive_arg = str_to_bool(sys.argv[2])
  word_arg = str(sys.argv[1])
  print(word_validation(word_arg, is_infinitive_arg))
else:
  print ("Arguments quantity wrong")

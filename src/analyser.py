# -*- coding: utf-8 -*-
#!/usr/bin/python

from lexicAnalyser import *
from sintaxAnalyser import *
from subprocess import check_output

import time

"""
    Authors: Jaelson Carvalho, Wesnydy Ribeiro
    Python >= 2.7
    Usage: python analyser.py
"""

def str_to_bool(s):
    if s == 'True':
         return True
    elif s == 'False':
         return False
    else:
         print("'", s, "'")
         raise ValueError # evil ValueError that doesn't tell you what the wrong value was

# "Realizando ligação para Jaelson"
print "Entre com a frase:"
sentence = raw_input()

lexicResult = runLexic(sentence)

gbRet = True

for w, t, b in lexicResult:
    if t != "artigo":
        ret = check_output(['python3', 'synonyms.py', w.encode('utf-8'), str(b).encode('utf-8')], shell=False)
        res = str_to_bool(ret.strip())
        if not res:
            gbRet = False

if not gbRet:
    print "Problema com sinônimo"
else:
    runSintax(lexicResult)
